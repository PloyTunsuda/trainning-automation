*** Settings ***
Library         Selenium2Library
Variables        config.yaml
Test Teardown   Close Browser

*** Keywords ***
Open markro website with environment
    [Arguments]     ${environment}
    IF    '${environment}'=='qa'                
        Open website    ${env["url"]["qa"]}
    ELSE IF    '${environment}'=='dev'          
        Open website    ${env["url"]["dev"]}
    ELSE IF    '${environment}'=='production'   
        Open website    ${env["url"]["prod"]}
    END

Open website
    [Arguments]     ${url}
    Open Browser	url=${url}     browser=Chrome
    Maximize Browser Window	

Close ad
    click element   xpath=//div[@id="eventPopupModal"]//a[@aria-label="Close"]
    Wait Until Page Contains Element      xpath=//div[@id="eventPopupModal" and @aria-hidden="true"]

Search product on markro
    [Arguments]     ${searching}
    click element   xpath=//input[@placeholder="ค้นหาสินค้า, หมวดหมู่, รหัสสินค้า"]
    Input Text      xpath=//input[@placeholder="ค้นหาสินค้า, หมวดหมู่, รหัสสินค้า"]     ${searching}${\n}

Print 5 product name
    FOR     ${items}    IN RANGE   1  6
        Get product name and product code   ${items}
    END

Verify product result display 
    Wait Until Page Contains Element    xpath=//div[contains(@class,"ProductCard__CardProduct")]

Get product name and product code
    [Arguments]    ${n}=${1}
    ${name}=    Get Text   xpath=(//div[contains(@class,"ProductCard__TitleMaxLines")])[${n}]
    ${code}=    Get Text   xpath=(//div[contains(@class,"ProductCard__TextCode")])[${n}]
    log to console     ${name} ${code}

*** Variables ***
&{product}    fridge=ตู้เย็น     tv=ทีวี      cleaner=เครื่องดูดฝุ่น

*** Test Cases ***
HW4
    Open markro website with environment    ${env["site"]["prod"]}
    Close ad
    Search product on markro    ${product.fridge} 
    Verify product result display 
    Print 5 product name

    
